﻿#variables
$ServerList = (Get-Content "$PSScriptRoot\MonitoredServers.txt")
$OutputSqlServerName = "PDC-DBA-D01"
$ExportDatabase = "SqlPerfMonitoring.dbo."
$ExportSqlTable = -Join ($ExportDatabase, "PerfData")

#Counter names are sql 2014 specific. Not guaranteed to work with other versions of SQL Server.
$counters = @(    
    "\Memory\Available MBytes",
    "\PhysicalDisk(*)\Avg. Disk sec/Read",
    "\PhysicalDisk(*)\Avg. Disk sec/Write",
    "\PhysicalDisk(*)\Disk Reads/sec",
    "\PhysicalDisk(*)\Disk Writes/sec",
    "\Processor(*)\% Processor Time",
    "\SQLServer:General Statistics\User Connections",
    "\SQLServer:Memory Manager\Memory Grants Pending",
    "\SQLServer:SQL Statistics\Batch Requests/sec",
    "\SQLServer:SQL Statistics\SQL Compilations/sec",
    "\SQLServer:SQL Statistics\SQL Re-Compilations/sec",
    "\System\Processor Queue Length",
    "\Paging File(_Total)\% Usage"
    )
      
# Get performance counter data
$ctr = Get-Counter -ComputerName $ServerList -Counter $counters -SampleInterval 10 -MaxSamples 60

# Make into a neat data table
$ctrOutput = $ctr | select-Object -ExpandProperty CounterSamples | 
    Select-Object   @{n='Server';   e={$_.Path.Split('\')[2] }},
                    @{n='Set';      e={$_.Path.Split('\')[4] }},
                    @{n='Counter';  e={$_.Path.Split('\')[5] }},
                    CookedValue,
                    Timestamp | 
    Out-DbaDataTable -IgnoreNull

# Push to Sql table
Write-DbaDataTable -InputObject $ctrOutput -SqlServer $OutputSqlServerName -Table $ExportSqlTable
