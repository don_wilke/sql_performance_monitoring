#every 15 minutes indefinitely
Unregister-ScheduledJob -Name SQLPerfMonDataCapture

Register-ScheduledJob -Name SQLPerfMonDataCapture -ScriptBlock {
    D:\SQLPerfMon\SQLPerfmon.ps1
    } -Trigger (new-jobtrigger -Once -At "08/30/2017 2:06PM" -RepetitionInterval (New-TimeSpan -Minutes 10) -RepetitionDuration ([timespan]::MaxValue)) `
    -ScheduledJobOption (New-ScheduledJobOption -WakeToRun -RunElevated) `
        -Credential corporate\appadmin_dw
