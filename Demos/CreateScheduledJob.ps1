#every 15 minutes indefinitely
Unregister-ScheduledJob -Name SQLPerfMonDataCapture

Register-ScheduledJob -Name SQLPerfMonDataCapture -ScriptBlock {
    D:\SqlSourceControl\Code\Powershell\SQLPerfMon\RunSQLPerfmon.ps1
    } -Trigger (new-jobtrigger -Once -At "08/26/2017 0AM" -RepetitionInterval (New-TimeSpan -Minutes 15) -RepetitionDuration ([timespan]::MaxValue)) `
    -ScheduledJobOption (New-ScheduledJobOption -WakeToRun -RunElevated) `
        -Credential corporate\appadmin_dw
