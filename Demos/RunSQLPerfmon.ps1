Set-Location $PSScriptRoot
$MonitoredServerList = Get-Content ".\MonitoredServers.txt"
$outputserver = "PDC-DBA-D01"

foreach ($item in $MonitoredServerList) {
  $sqlservername = $item
    Start-Job -FilePath .\SQLPerfmon.ps1 -ArgumentList @($sqlservername, $outputserver, (get-location))
      }