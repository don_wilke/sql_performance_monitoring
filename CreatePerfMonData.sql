USE [SqlPerfMonitoring]
GO
DROP TABLE [dbo].[PerfData]
GO
DROP SEQUENCE [dbo].[PerfDataId] ;
GO
CREATE SEQUENCE [dbo].[PerfDataId] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE  1000 
 ;
GO
CREATE TABLE [dbo].[PerfData](
	[Server] [nvarchar](max) NULL,
	[Set] [nvarchar](max) NULL,
	[Counter] [nvarchar](max) NULL,
	[CookedValue] [float] NULL,
	[Timestamp] [datetime] NULL,
	Id Bigint NOT NULL Default (Next Value for perfdataid) Primary Key
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO